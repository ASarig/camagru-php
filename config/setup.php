<?php

$conn = new mysqli("localhost", "root", "338620");

$sql = "CREATE DATABASE IF NOT EXISTS camagru";

$conn->query($sql);

$sql = "USE `camagru`";

$conn->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `Users` (
	`user_id` INT NOT NULL AUTO_INCREMENT,
	`username` varchar(20) NOT NULL UNIQUE,
	`password` TEXT NOT NULL,
	`email` varchar(50) NOT NULL,
	`conf_code` varchar(1000) NOT NULL,
	`active` BOOLEAN NOT NULL DEFAULT '0',
	`fullname` varchar(100) NOT NULL,
	`profile_pic` TEXT NOT NULL,
	PRIMARY KEY (`user_id`)
);";

$conn->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `Posts` (
	`post_id` INT NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`pic_location` TEXT NOT NULL,
	`description` TEXT NOT NULL,
	`post_date` TIMESTAMP NOT NULL,
	PRIMARY KEY (`post_id`)
);";

$conn->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `Likes` (
	`like_id` INT NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`post_id` INT NOT NULL,
	PRIMARY KEY (`like_id`)
);";

$conn->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `Comments` (
	`comment_id` INT NOT NULL AUTO_INCREMENT,
	`post_id` INT NOT NULL,
	`user_id` INT NOT NULL,
	`comment` TEXT NOT NULL,
	PRIMARY KEY (`comment_id`)
);";

$conn->query($sql);

$sql = "ALTER TABLE `Posts` ADD CONSTRAINT `Posts_fk0` FOREIGN KEY (`user_id`) REFERENCES `Users`(`user_id`);";

$conn->query($sql);

$sql = "ALTER TABLE `Likes` ADD CONSTRAINT `Likes_fk0` FOREIGN KEY (`user_id`) REFERENCES `Users`(`user_id`);";

$conn->query($sql);

$sql = "ALTER TABLE `Likes` ADD CONSTRAINT `Likes_fk1` FOREIGN KEY (`post_id`) REFERENCES `Posts`(`post_id`);";

$conn->query($sql);

$sql = "ALTER TABLE `Comments` ADD CONSTRAINT `Comments_fk0` FOREIGN KEY (`post_id`) REFERENCES `Posts`(`post_id`);";

$conn->query($sql);

$sql = "ALTER TABLE `Comments` ADD CONSTRAINT `Comments_fk1` FOREIGN KEY (`user_id`) REFERENCES `Users`(`user_id`);";

$conn->query($sql);

$conn->close();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Camagru</title>
</head>
<body>
<form action="../index.php">
	<button autofocus="autofocus" tabindex="1">Index</button>
</form>
</body>
</html>
