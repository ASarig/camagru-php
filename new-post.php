<?php
include('includes/include.php');
include('includes/header.php');

$db = new Database();
if (isset($_COOKIE['user'])) {
  $user = unserialize($_COOKIE['user']);
} else if (isset($_SESSION['user'])) {
  $user = unserialize($_SESSION['user']);
} else {
  $user = new User();
}

if ($user->isLogged() === FALSE) {
  header('Location: login.php');
}

if (isset($_GET['del-photo'])) {
  if (file_exists($_GET['del-photo'])) {
    unlink($_GET['del-photo']);
  }
}

?>
<!DOCTYPE html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<link rel="stylesheet" href="takepic.css">
<div class="imframe">
  <div>
    <div id="camera-container">
      <div id="camera_info"></div>
      <div id="camera"></div><br>
      <button id="take_snapshots" class="caption-button">Take Snapshots</button>
      <div class="library">
        <p>Or upload your photo:</p>
        <form action="action.php" method="post" enctype="multipart/form-data" class="uploadform">
          <input class="uploadbutton" type="file" name="file">
          <button type="submit" name="upload" class="filterbutton">Upload photo</button>
        </form>
      </div>
    </div>
  </div>
  <div class="rightbar">
    <form action="#" method="POST" class="filterform">
      <select name="filter">
        <option value='1'>Frame1</option>
        <option value='2'>Frame</option>
        <option value='3'>Trump</option>
      </select>
      <input type="submit" name="take" class="filterbutton" value="Choose filter" />
      <br>
      <?php
      if(isset($_POST['take']) && isset($_SESSION['picture'])){
        if (isset($_POST['filter']) && !empty($_POST['filter'])) {
          $_SESSION['picture'] = $_POST['filter'];
          if($_SESSION['picture'] == '1'){
            echo 'Selected filter: Glass';
            $_SESSION['picture'] = 'img/frame1.png';
          } else if($_SESSION['picture'] == '2'){
            echo 'Selected filter: Grass';
            $_SESSION['picture'] = 'img/grass.png';
          } else if($_SESSION['picture'] == '3'){
            echo 'Selected filter: Easter';
            $_SESSION['picture'] = 'img/easter.png';
          }
        }
      } else{
        echo "<script>alert('No filter selected');</script>";
      }

      ?>
    </form>
    <img width="50px" src="img/frame1.png" class="filterimages">
    <img width="50px" src="img/grass.png" class="filterimages">
    <img width="50px" src="img/easter.png" class="filterimages">
    <br />
  </div>
</div>
<div id="tmp-images">
  <table id="img-table">
    <thead>
      <tr>
        <th>Image</th><th>Image Name</th><th>Options</th>
      </tr>
    </thead>
    <tbody id="imagelist">

    </tbody>
  </table>
</div>
</body>
</html>
<style>
#camera {
  width: 100%;
  height: 350px;
}

</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="jpeg_camera/jpeg_camera_with_dependencies.min.js" type="text/javascript"></script>
<script>
var options = {
  shutter_ogg_url: "jpeg_camera/shutter.ogg",
  shutter_mp3_url: "jpeg_camera/shutter.mp3",
  swf_url: "jpeg_camera/jpeg_camera.swf",
};
var camera = new JpegCamera("#camera", options);

$('#take_snapshots').click(function(){
  var snapshot = camera.capture();
  snapshot.show();

  snapshot.upload({api_url: "action.php"}).done(function(response) {
    $('#imagelist').prepend("<tr><td><img class='tmp-img' src='"+response+"'></td><td>"+response+"</td><td><a href='new-post.php?del-photo="+response+"'><button class='delete-button'>Delete</button><a href='post-photo.php?photo="+response+"'><button class='post-button'>Post</button></a></td></tr>");
  }).fail(function(response) {
    alert("Upload failed with status " + response);
  });
})

function done(){
  $('#snapshots').html("uploaded");
}
</script>
