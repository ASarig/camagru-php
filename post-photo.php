<?php
include('includes/header.php');

$db = new Database();
if (isset($_COOKIE['user'])) {
  $user = unserialize($_COOKIE['user']);
} else if (isset($_SESSION['user'])) {
  $user = unserialize($_SESSION['user']);
} else {
  $user = new User();
}

if (empty($user)) {
  header('Location: login.php');
} else if ($user->isLogged() === FALSE) {
  header('Location: login.php');
}

if (isset($_POST['submit'])) {
  $userid = $user->getUserID();
  $name = preg_replace('/\s+/', '', basename($_SESSION['imgPath']) . PHP_EOL);
  $newPath = 'uploads/'.$user->getUserName().'/'.$name;
  rename($_SESSION['imgPath'], $newPath);
  $description = $_POST['description'];
  $sql = "INSERT INTO Posts (post_id,user_id,pic_location,description,post_date) VALUES ('','$userid','$newPath','$description',date('YmdHis'))";
  $db->query($sql);
  $_SESSION['imgPath'] = null;
}

if (!isset($_GET['photo'])) {
#  header('Location: new-post.php');
} else {
  $_SESSION['imgPath'] = $_GET['photo'];
  echo "
<div class='new-post'>
  <form method='POST' action='post-photo.php' id='new-post'>
  <img src='" . $_GET['photo'] . "' />
  <textarea name='description' placeholder='Description' form='new-post'></textarea>
  <input id='post' type='submit' name='submit'>
  </form>
</div>
  ";
}
 ?>
