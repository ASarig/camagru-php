<?php
include('includes/include.php');
include('includes/header.php');

$db = new Database();
if (isset($_COOKIE['user'])) {
  $user = unserialize($_COOKIE['user']);
} else if (isset($_SESSION['user'])) {
  $user = unserialize($_SESSION['user']);
} else {
  $user = new User();
}

if (isset($_GET['like'])) {
  $user->like($_GET['like'], $db);
  header('Location: index.php?#' . $_GET['like']);
}

if (isset($_GET['delete-post'])) {
  $sql = "SELECT user_id, pic_location FROM posts WHERE post_id='" . $_GET['delete-post'] . "'";
  $delResult = $db->query($sql);
  if (!empty($delResult)) {
  if ($delResult[0]['user_id'] == $user->getUserID()) {
    $sql1 = "DELETE FROM likes WHERE post_id='" . $_GET['delete-post'] . "'";
    $sql2 = "DELETE FROM comments WHERE post_id='" . $_GET['delete-post'] . "'";
    $sql3 = "DELETE FROM posts WHERE post_id='" . $_GET['delete-post'] . "'";
    $db->query($sql1);
    $db->query($sql2);
    $db->query($sql3);
    if (file_exists($delResult[0]['pic_location'])) {
      unlink($delResult[0]['pic_location']);
    }
    echo "<script>alert('Post successfully deleted!');</script>";
  }
}
}

if (isset($_GET['user'])) {
  if (isset($_GET['page'])) {
    $sql = "SELECT * FROM posts WHERE user_id=" . $_GET['user'] . " ORDER BY post_date DESC OFFSET " . $_GET['page'] . " FETCH NEXT " . $_GET['page'] + 10 . " ROWS ONLY";
  } else {
  $sql = "SELECT * FROM posts WHERE user_id=" . $_GET['user'] . " ORDER BY post_date DESC OFFSET 0 FETCH NEXT 10 ROWS ONLY";
}
  $result = $db->query($sql);
} else {
  if (isset($_GET['page'])) {
  $sql = "SELECT * FROM posts ORDER BY post_date DESC LIMIT 10 OFFSET " . intval($_GET['page'] * 10 - 10);
} else {
    $sql = "SELECT * FROM posts ORDER BY post_date DESC LIMIT 10 OFFSET 0";
  }
  $result = $db->query($sql);
}

foreach($result as $post) {
  echo "
  <div class='post-container' id='" . validate($post['post_id']) . "'>
  <div class='post-image'>
  ";
  if ($post['user_id'] == $user->getUserID()) {
    echo "<button class='delete' onclick=\"location.href='index.php?delete-post=" . validate($post['post_id']) . "'\")'> Delete Post </button>";
  }
  echo "
  <img class='post-img' src='" . $post['pic_location'] . "'>
  </div>
  <div class='post-title-bar'>
  <a href='index.php?user=" . validate($post['user_id']) . "'>
  <img class='profile-pic' src='img/profile-pic.jpg'>
  <div>";
  $postBy = $db->query("SELECT fullname FROM users WHERE user_id=" . $post['user_id']);
  echo validate($postBy[0]['fullname']);
  echo "</div>
  </a>
  </div>
  <div class='post-details'>
  <p class='description'>
  " . validate($post['description']) . "
  </p>";
  $likes = $db->query("SELECT * FROM likes WHERE post_id=" . $post['post_id']);
  $comments = $db->query("SELECT * FROM comments WHERE post_id=" . $post['post_id']);
  echo count($likes, 0) . " likes  " . count($comments, 0) . " comments";
  echo "</div>
  <div class='post-actions'>
  <a href='index.php?like=" . $post['post_id'] . "#" . $post['post_id'] . "'><button";
  if ($user->isLogged()) {
  if (!empty($db->query("SELECT * FROM likes WHERE user_id=" . $user->getUserID() . " AND post_id=" . $post['post_id']))) {
    echo " style='background-color:blue;'";
  }
}
  echo " class='like'><img src='img/like-icon.png' /> Like </button></a>
  <button class='comment' onclick='comment(" . $post['post_id'] . ")'><img src='img/comment-icon.png' /> Comment </button>
  </div>
  </div>";
}
?>
<div class='paginator'>
  <?php if (count($result) == 10) {
    echo "
  <div class='next'>
    <a href='index.php?page=";
    if (isset($_GET['page'])) {
      echo $_GET['page'] + 1;
    } else {
      echo '2';
    }
    echo "'>
      <p> Next Page </p>
    </a>
  </div>";
}
if (isset($_GET['page'])) {
  if ($_GET['page'] > 1) {
    echo "
  <div class='previous'>
    <a href='index.php?page=";
    if ($_GET['page'] > 1) {
      echo $_GET['page'] - 1;
    }
    echo "'>
      <p> Previous Page </p>
    </a>
  </div>";
}
}?>
</div>
<div id="comments">

</div>
<script>
function comment($postid) {
  document.getElementById("comments").innerHTML = "<button onclick='closeComment()'>X</button><iframe id='show-comments' src='comments.php?post=" + $postid + "'></iframe>";
}

function closeComment() {
  document.getElementById("comments").innerHTML = "";
}
</script>
