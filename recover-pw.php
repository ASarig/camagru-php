<?php
  session_start();
  include('includes/include.php');
  $db = new Database();
  if (isset($_COOKIE['user'])) {
    $user = unserialize($_COOKIE['user']);
  } else if (isset($_SESSION['user'])) {
    $user = unserialize($_SESSION['user']);
  } else {
    $user = new User();
  }

  if ($user->isLogged()) {
    header('Location: index.php');
  }

  if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $result = $db->query("SELECT * FROM users WHERE email='$email'");
    if (empty($result)) {
      echo "<script>alert('This email address is not registered!');</script>";
    } else {
      $user->forgottenPassword($_POST['email'], $db);
      echo "<script>alert('A password recovery link was sent to your email!');</script>";
    }
  }

  if (isset($_POST['update'])) {
    if ($_POST['new-pw1'] !== $_POST['new-pw2']) {
      echo "<script>alert('The two password does not match!');</script>";
    } else {
    $user->setForgottenPassword($_GET['activation_code'], $_POST['new-pw1'], $db);
  }
  }
 ?>
<!DOCTYPE HTML>
<html>
<head>
  <link rel='stylesheet' type="text/css" href="css/style.css" />
</head>
<body>
  <div class="container">
    <div class="center-form">
 <div class="form-container">
   <?php if (!isset($_GET['recovery-code'])) {
     echo "
  <form method='post' action='recover-pw.php'>
    <input type='email' value='' name='email' placeholder='Email'/>
    <input type='submit' name='submit' value='Recover password'>
  </form>";
} else {
  $result = $db->query("SELECT * FROM users WHERE conf_code=" . $_GET['recovery-code']);
  if (!empty($result)) {
    echo "
    <form method='post' action='recover-pw.php'>
      <input type='password' value='' name='new-pw1' placeholder='Password'/>
      <input type='password' value='' name='new-pw2' placeholder='Password Again'/>
      <input type='submit' name='update' value='Set new password'>
    </form>
    ";
  } else {
    echo "<script>alert('Your password recovery code is not valid! Please try again!');</script>";
  }
}
?>
</div>
</div>
</div>
</body>
</html>
