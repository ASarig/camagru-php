<?php
include('includes/include.php');

session_start();
$db = new Database();
if (isset($_COOKIE['user'])) {
  $user = unserialize($_COOKIE['user']);
} else if (isset($_SESSION['user'])) {
  $user = unserialize($_SESSION['user']);
} else {
  $user = new User();
}

if ($user->isLogged()) {
  header('Location: index.php');
}

if (isset($_POST['submit'])) {
  if (!empty($_POST['fullname']) && !empty($_POST['username']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['passwordagain'])) {
    $dataArray = array('fullname'=>$_POST['fullname'], 'username'=>$_POST['username'], 'email'=>$_POST['email'], 'password'=>$_POST['password']);
    if ($dataArray['password'] != $_POST['passwordagain']) {
      echo "<script>alert('The two password does not match!');</script>";
    } else if ($user->register($dataArray, $db)) {
    $_SESSION['user'] = serialize($user);
    header('Location: signup.php?status=successfull');
  }
  }
}
?>
<!DOCTYPE HTML>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
  <div class='container'>
    <div class='center-form'>
  <?php if (!isset($_GET['status']) && !isset($_GET['activate'])) { echo "
  <div class='form-container'>
    <form method='post' action='signup.php'>
      <input type='text' value='' name='fullname' placeholder='Full Name'/>
      <input type='text' value='' name='username' placeholder='Username'/>
      <input type='text' value='' name='email' placeholder='E-mail'/>
      <input type='password' value='' name='password' placeholder='Password'/>
      <input type='password' value='' name='passwordagain' placeholder='Password Again'/>
      <input type='submit' name='submit' value='Registration'>
    </form>
  </div>";
} else if (isset($_GET['status']) && $_GET['status'] === 'successfull') {
  echo "<script>alert('Signup successfull, now you can login!');</script>";
} else if (isset($_GET['activate'])) {
  if ($user->activate($_GET['activate'], $db) === true) {
    echo "<script>alert('Account successfully activated!');</script>";
  } else {
    echo "<script>alert('Something went wrong, please try again!');</script>";
  }
}
  ?>
</div>
</div>
</body>
</html>
