<?php
include('includes/include.php');

  session_start();
  $db = new Database();
  if (isset($_COOKIE['user'])) {
    $user = unserialize($_COOKIE['user']);
  } else if (isset($_SESSION['user'])) {
    $user = unserialize($_SESSION['user']);
  } else {
    $user = new User();
  }


    if ($user->isLogged() === TRUE) {
      header('Location: index.php');
    }

  if (isset($_POST['submit'])) {
    if (isset($_POST['username']) && isset($_POST['password'])) {
      if ($user->login($_POST['username'], $_POST['password'], $db)) {
        $_SESSION['user'] = serialize($user);
        header('Location: index.php');
    } else {
      echo "<script>alert('Username or password is not correct!');</script>";
    }
    }
  }

 ?>
<!DOCTYPE HTML>
<html>
<head>
  <link rel='stylesheet' type="text/css" href="css/style.css" />
</head>
<body>
  <div class="container">
    <div class="center-form">
 <div class="form-container">
  <form method="post" action="login.php">
    <input type="text" value="" name="username" placeholder="Username"/>
    <input type="password" value="" name="password" placeholder="Password"/>
    <input type="radio" name="remember" />Remember me
    <input type="submit" name="submit" value="Bejelenktez&eacute;s">
    <a href="signup.php"><p>Registration</p></a>
    <a href="recover-pw.php"><p>I forgot my password</p></a>
  </form>
</div>
</div>
</div>
</body>
</html>
