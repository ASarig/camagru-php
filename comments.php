<link rel='stylesheet' type="text/css" href="css/style.css">
<div class='comment-body'>
  <div class='comment-cont'>
<?php
session_start();
include('includes/include.php');

$db = new Database();
if (isset($_COOKIE['user'])) {
  $user = unserialize($_COOKIE['user']);
} else if (isset($_SESSION['user'])) {
  $user = unserialize($_SESSION['user']);
} else {
  $user = new User();
}

if (isset($_GET['delete-comment'])) {
  if ($user->isLogged()) {
  $user->deleteComment($_GET['post'], validate($_GET['delete-comment']), $db);
}
}

if (isset($_POST['submit'])) {
  $user->sendComment($_GET['post'], $_POST['comment'], $db);
}

if (isset($_GET['post'])) {
  $sql = "SELECT * FROM comments WHERE post_id='" . $_GET['post'] . "'";
  $comments = $db->query($sql);
  foreach($comments as $comment) {
    $sql2 = "SELECT * FROM users WHERE user_id='" . $comment['user_id'] . "'";
    $commentBy = $db->query($sql2);
    echo "
    <div class='comment-container'>
      <div class='user'>
        <img src='img/profile-pic.jpg' />
        <h6>" . validate($commentBy[0]['fullname']) . "</h6>
      </div>
      <div class='comment'>
         <p>" . validate($comment['comment']) . "</p>
      </div>";
      if ($comment['user_id'] == $user->getUserID()) {
      echo "
      <button onclick=\"location.href='comments.php?delete-comment=" . $comment['comment_id'] . "&post=" . $_GET['post'] . "'\")'> Delete </button>";
    }
    echo "
    </div>
    ";
  }
}
 ?>
</div>
 <div class='new-comment'>
   <form id='newcomment' method='post' action='comments.php?post=<?php echo $_GET['post'];?>'>
     <textarea name='comment' form='newcomment' placeholder='Write your comment here...'></textarea>
     <input type='submit' name='submit'>
   </form>
 </div>
</div>
