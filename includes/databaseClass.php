<?php
class Database {
  private $conn;

  public function __construct() {
    if (file_exists('../config/database.php')) {
    include_once('../config/database.php');
  } else {
    include_once('config/database.php');
  }
    $this->conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
    $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function query($sql) {
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();

    if (strpos($sql, 'INSERT') === FALSE && strpos($sql, 'UPDATE') === FALSE && strpos($sql, 'DELETE') === FALSE && strpos($sql, 'CREATE') === FALSE) {
      $toReturn = $stmt->fetchAll();
      return $toReturn;
    }
  }
}
?>
