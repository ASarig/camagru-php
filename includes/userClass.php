<?php
class User {
  private $userID;
  private $userName;
  private $identity;
  private $fullName;
  private $userEmail;

  public function __construct() {
  }

  public function login($username, $password, $db) {
    $hashed_password = hash("whirlpool", $password);
    $sql = "SELECT * FROM users WHERE username='" . $username . "' AND password='" . $hashed_password . "'";

    $result = $db->query($sql);

    if (!empty($result[0]['username'])) {
      $this->userID = $result[0]['user_id'];
      $this->userName = $result[0]['username'];
      $this->identity = $result[0]['password'];
      $this->userEmail = $result[0]['email'];
      $this->fullName = $result[0]['fullname'];
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function register($dataArray, $db) {
    $name = validate($dataArray['fullname']);
    $username = validate($dataArray['username']);
    $email = validate($dataArray['email']);
    $password = $dataArray['password'];

    $hashed_pw = hash('whirlpool', $password);
    $activation_code = hash('whirlpool', rand(0, 1000));

    if (!empty($db->query("SELECT * FROM users WHERE username='$username'"))) {
      echo "<script>alert('This username already exists!');</script>";
      return FALSE;
    } else if (!empty($db->query("SELECT * FROM Users WHERE email='$email'"))) {
      echo "<script>alert('This email is already registered!');</script>";
      return FALSE;
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      echo "<script>alert('Please enter a valid email address!');</script>";
      return FALSE;
    } else if (strlen($password) < 7) {
      echo "<script>alert('Your password must contain more than 7 characters!');</script>";
      return FALSE;
    } else if (!preg_match("#[a-zA-Z]+#", $password) && !preg_match("#[0-9]+#", $password)) {
      return FALSE;
      echo "<script>alert('Your password must contain at least one small letter, one capital letter and a number!');</script>";
    } else {
      $usrDir = "uploads/$username/";
      $usrTmpDir = "uploads/$username/tmp";
      mkdir($usrDir);
      mkdir($usrTmpDir);
      $db->query("INSERT INTO users (fullname,username,password,email,conf_code) VALUES ('$name','$username','$hashed_pw','$email','$activation_code')");
      $code = "?activate=" . $activation_code;
      sendMail($email, $code);
      return TRUE;
    }
  }

  public function activate($activation_code, $db) {
    if (!empty($db->query("SELECT * FROM Users WHERE conf_code='" . $activation_code . "'"))) {
      $db->query("UPDATE Users SET active='1', conf_code='" . hash('whirlpool', rand(0, 1000)) . "' WHERE conf_code='$activation_code'");
      return true;
    }
    return false;
  }

  public function forgottenPassword($email, $db) {
    $result = $db->query("SELECT * FROM Users WHERE email='" . $email . "'");
    if (!empty($result)) {
      $code = "?recovery-code=" . $result[0]['conf_code'];
      $email = $result[0]['email'];
      sendMail($email, $code);
    }
  }

  public function setForgottenPassword($new_pw_code, $new_pw, $db) {
    if (strlen($new_pw) < 7) {
      echo "<script>alert('Your password must contain more than 7 characters!');</script>";
      return FALSE;
    } else if (!preg_match("#[a-zA-Z]+#", $new_pw) && !preg_match("#[0-9]+#", $new_pw)) {
      return FALSE;
      echo "<script>alert('Your password must contain at least one small letter, one capital letter and a number!');</script>";
    } else {
      if (!empty($db->query("SELECT * FROM users WHERE conf_code='" . $new_pw_code . "'"))) {
        $db->query("UPDATE users SET password='" . hash('whirlpool', $new_pw) . "', conf_code='" . hash('whirlpool', rand(0, 1000)) . "' WHERE conf_code='$new_pw_code'");
        return TRUE;
      }
    }
  }

  public function isLogged() {
    if (!empty($this->userName) && !empty($this->identity)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function updatePassword($newPassword, $db) {
    if ($this->isLogged() === TRUE) {
      if (strlen($newPassword) < 7) {
        echo "<script>alert('Your password must contain more than 7 characters!');</script>";
      } else if (!preg_match("#[a-zA-Z]+#", $newPassword) && !preg_match("#[0-9]+#", $newPassword)) {
        echo "<script>alert('Your password must contain at least one small letter, one capital letter and a number!');</script>";
      } else {
        echo "<script>alert('Password updated successfully!');</script>";
        $db->query("UPDATE users SET password='" . hash('whirlpool', $newPassword) . "' WHERE username='$this->userName'");
      }
    }
  }

  public function updateUserName($newUsername, $db) {
    if ($this->isLogged() === TRUE) {
      if (!empty($db->query("SELECT * FROM users WHERE username='$newUsername'"))) {
        echo "<script>alert('This username already exists!');</script>";
      } else {
        $db->query("UPDATE users SET username='" . $newUsername . "' WHERE username='$this->userName'");
        $this->userName = $newUsername;
        echo "<script>alert('Username updated successfully!');</script>";
      }
    }
  }

  public function updateEmail($newEmail, $db) {
    if ($this->isLogged()) {
      if (!filter_var($newEmail, FILTER_VALIDATE_EMAIL)) {
        echo "<script>alert('Please enter a valid email address!')";
      } else {
        $db->query("UPDATE users SET email='" . $newEmail . "' WHERE username='$this->userName'");
        $this->userEmail = $newEmail;
        echo "<script>alert('Email updated successfully!');</script>";
      }
    } else {
      echo "<script>alert('Pease log in first!');</script>";
    }
  }

  public function like($post_id, $db) {
    if ($this->isLogged()) {
      if (empty($db->query("SELECT * FROM likes WHERE user_id=" . $this->getUserID() . " AND post_id=" . $post_id))) {
        $db->query("INSERT INTO Likes (user_id,post_id) VALUES ('$this->userID','$post_id')");
      } else {
        $db->query("DELETE FROM Likes WHERE user_id='$this->userID' AND post_id='$post_id'");
      }
    } else {
      echo "<script>alert('You have to login first!');</script>'";
    }
  }

  public function sendComment($post_id, $comment, $db) {
    if ($this->isLogged()) {
      $comment = validate($comment);
      $post_id = validate($post_id);
      $sql = "INSERT INTO comments (post_id,user_id,comment) VALUES ('$post_id','" . $this->getUserID() . "','$comment')";
      $db->query($sql);
    } else {
      echo "<script>alert('You have to log in first!');</script>";
    }
  }

  public function deleteComment($post_id, $comment_id, $db) {
    $db->query("DELETE FROM comments WHERE user_id='$this->userID' AND post_id='$post_id' AND comment_id='$comment_id'");
    echo "<script>alert('Comment successfully deleted!');</script>";
  }

  public function getUserID() {
    return $this->userID;
  }

  public function getUserName() {
    return $this->userName;
  }

  public function getFullName() {
    return $this->fullName;
  }

  public function getUserEmail() {
    return $this->userEmail;
  }
}
?>
