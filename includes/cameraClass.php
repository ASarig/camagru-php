<?php
class cameraClass {
  private $imageFolder;
  private $db;
  private $user;

  public function __construct($db, $user) {
    $this->db = $db;
    $this->user = $user;
    $this->imageFolder = "uploads/" . $this->user->getUserName() . "/tmp/";
  }

  //This function will create a new name for every image captured using the current data and time.
  public function getNameWithPath() {
    $name = $this->imageFolder . date('YmdHis') . ".png";
    return $name;
  }

  //function will get the image data and save it to the provided path with the name and save it to the database
  public function showImage() {
    $userid = $this->user->getUserID();
    $nameWithPath = $this->getNameWithPath();
    $file1 = file_get_contents('php://input');
    if (empty($file1)) {
      $file1 = file_get_contents($_FILES['file']['tmp_name']);
    }
    $file = file_put_contents($nameWithPath, $file1);
    $file2 = $this->combineImages($nameWithPath);
    if(!$file){
      return "ERROR: Failed to write data to " . $nameWithPath . ", check permissions\n";
    } else {
      return $this->getNameWithPath();
    }

  }

  //function for changing the image to base64
  public function changeImagetoBase64($image){
    $path = $image;
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
    return $base64;
  }


  public function combineImages($imageurl){

    $image1 = $imageurl;
    $image2 = $_SESSION['picture'];

    list($width, $height) = getimagesize($image2);

    $image1=imagecreatefromjpeg($image1);
    $image2=imagecreatefrompng($image2);
    imagealphablending( $image2, false );
    imagesavealpha( $image2, true );

    imagecopyresampled($image1, $image2, 0, 0, 0, 0, $width, $height, $width, $height);
    unlink($imageurl);
    imagejpeg($image1,'uploads/' . $this->user->getUserName() . '/tmp/' . date('YmdHis') . '.png');

    return $image1;
  }
}
