<?php
session_start();
include_once('include.php');
if (isset($_COOKIE['user'])) {
  $user = unserialize($_COOKIE['user']);
} else if (isset($_SESSION['user'])) {
  $user = unserialize($_SESSION['user']);
} else {
  $user = new User();
}
  echo "
  <!DOCTYPE HTML>
  <html>
  <head>
    <link rel='stylesheet' type='text/css' href='css/style.css' />
  </head>
  <body>
  <div class='header'>
    <div class='headeruser'>
";

  if (!empty($user)) {
  if ($user->isLogged() === TRUE) {
    echo "<img src='img/profile-pic.jpg'>
    <a href='settings.php'>
    <h5>" . $user->getFullName() . "</h5>
    </a>
    ";
  } else {
    echo "<a href='login.php'><h5>Login</h5></a>";
  }
} else {
  echo "<a href='login.php'><h5>Login</h5></a>";
}
echo "
</div>
    <div>
    <div class='menu'>
      <ul class='main-navigation'>
        <li id='first'><a href='index.php '>Home</a></li>
        <li><a href='new-post.php'>New post</a></li>
        <li id='logout'><a href='logout.php'>Logout</a></li>
      </ul>
    </div>
  </div>
  </div>
  ";
 ?>
