<?php
function validate($string) {
  $string = trim($string);
  $string = stripslashes($string);
  $string = htmlspecialchars($string);
  $string = htmlspecialchars($string, ENT_QUOTES);
  return $string;
}

function sendMail($to, $code) {
  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $headers .= 'From: <info@csikevents.ro>' . "\r\n";
  if (strpos($code, 'activate')) {
    $subject = "Account activation";
    $message = "  <div>
    <a href='signup.php" . $code . "'><p>LINK</p></a>
    </div>";
  } else if (strpos($code, 'recovery-code')) {
    $subject = "Password recovery";
    $message = "  <div>
    <a href='recover-pw.php" . $code . "'><p>LINK</p></a>
    </div>";
  }
  if (!empty($message)) {
    mail($to,$subject,$message,$headers);
  }
}
?>
