<?php
include('includes/include.php');
include('includes/header.php');

$db = new Database();
if (isset($_COOKIE['user'])) {
  $user = unserialize($_COOKIE['user']);
} else if (isset($_SESSION['user'])) {
  $user = unserialize($_SESSION['user']);
} else {
  $user = new User();
}

if ($user->isLogged() === FALSE) {
  header('Location: login.php');
}

  if (isset($_POST['update-password'])) {
    if ($_POST['pw1'] === $_POST['pw2']) {
      $user->updatePassword($_POST['pw1'], $db);
    } else {
      echo "<script> alert('The two passwords does not match!'); </script>";
    }
  } else if (isset($_POST['update-username'])) {
    $user->updateUserName($_POST['username'], $db);
  } else if (isset($_POST['update-email'])) {
    $user->updateEmail($_POST['email'], $db);
  }
 ?>
<div class='container'>
  <div class='center-form'>
 <form method='post' action='settings.php'>
   <strong> Username: </strong>
   <input type='text' name='username' value='<?php echo $user->getUserName(); ?>' />
   <input type='submit' name='update-username' value='Update Username' />
 </form>
 <form method='post' action='settings.php'>
   <strong> E-mail address: </strong>
   <input type='text' name='email' value='<?php echo $user->getUserEmail(); ?>' />
   <input type='submit' name='update-email' value='Update Email Address' />
 </form>
 <form method='post' action='settings.php'>
   <strong> Password: </strong>
   <input type='password' name='pw1' value='' />
   <input type='password' name='pw2' value='' />
   <input type='submit' name='update-password' value='Update Password' />
 </form>
</div>
</div>
