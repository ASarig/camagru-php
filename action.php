<?php
session_start();
include_once('includes/include.php');
$db = new Database();
if (isset($_COOKIE['user'])) {
  $user = unserialize($_COOKIE['user']);
} else if (isset($_SESSION['user'])) {
  $user = unserialize($_SESSION['user']);
} else {
  $user = new User();
}

$webcamClass = new cameraClass($db, $user);

if (empty($_FILES['file'])) {
echo $webcamClass->showImage();
} else {
  $webcamClass->showImage();
  header('Location: post-photo.php?photo=' . $webcamClass->getNameWithPath());
}
